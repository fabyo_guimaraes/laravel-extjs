
Ext.define('LaraExt.view.Viewport', {
    extend  : 'Ext.Viewport'
    ,layout : 'fit'

    ,initComponent: function() {
        var me = this;
        
        Ext.apply(me, {
            items: [
                {
                    xtype: 'funcionariogrid'
                }
            ]
        });
                
        me.callParent(arguments);
    }
});