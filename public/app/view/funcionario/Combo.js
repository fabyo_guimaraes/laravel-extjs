Ext.define('LaraExt.view.funcionario.Combo', {
    extend          : 'Ext.form.field.ComboBox'
    ,alias          : 'widget.funcionariocombo'
	,editable       : false
    ,displayField   : 'valor'
    ,valueField     : 'id'
	,forceSelection : true
	,multiSelect    : true
    ,mode           : 'local'
    ,triggerAction  : 'all'
    ,listConfig     : {
		getInnerTpl : function(val) {
			return '<div class="x-combo-list-item"><img src="' + Ext.BLANK_IMAGE_URL + '" class="chkCombo-default-icon chkCombo" /> {valor}</div>';
     	}
    }
    ,store: {
        fields: ['id', 'valor']
        ,data: [
            {id: 'VT', valor: 'Transporte'}
            ,{id: 'CO', valor: 'Odonto'}
            ,{id: 'CM', valor: 'Medico'}
        ]
    }
});