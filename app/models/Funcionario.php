<?php

use LaravelBook\Ardent\Ardent;

class Funcionario extends Ardent
{
	public $timestamps = false;
	
	protected $guarded  = array('id');
	protected $fillable = array('nome', 'data_nascimento', 'beneficio', 'salario', 'cpf', 'sexo', 'ativo');

	public static $customMessages = array(
		'required' => 'Digite o campo :attribute.',
		'between' => 'O :attribute deve ter no minimo :min digitos'
	);

	public static $rules = array(
		'nome' => 'required|min:5',
		'ativo' => 'required'
	);
	
	public function beforeSave()
	{
		if($this->isDirty('nome')) {
			$retorno  = array();
			$string   = strtolower(trim(preg_replace("/\s+/", " ", $this->nome)));
			$palavras = explode(" ", $string);

			$retorno[] = ucfirst($palavras[0]);
			unset($palavras[0]);

			foreach ($palavras as $palavra){
				if (!preg_match("/^([dn]?[aeiou][s]?|em)$/i", $palavra)){
					$palavra = ucfirst($palavra);
				}
				$retorno[] = $palavra;
			}
			$this->nome = implode(' ', $retorno);
		}

		if($this->isDirty('beneficio')) {
			$this->beneficio = implode(',', $this->beneficio);
		}   
    }
}